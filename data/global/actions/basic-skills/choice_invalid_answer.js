//CHECKSUM:968ae131771756da23246adfc1a07271535563ca48cddb09940b13106836fc5c
"use strict";

/**
 * @hidden true
 */
const key = 'skill-choice-invalid-count';
const value = (temp[key] || 0) + 1;
temp[key] = value;
//# sourceMappingURL=choice_invalid_answer.js.map